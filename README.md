# Crybaby Slack error ping

This is a simple port of ARIA's simple error alerting tool to the FAE framework. 

When installed (and configured) it will send the details of any fatal errors to a nominated slack channel, so 
that they can be seen by the dev/ops team. 

This is no replacement for full fat monitoring, but is a very simple fallback for situations where full fat monitoring
isn't available, or has broken.

## Example basic usage

In the environment, configure `CRYBABY_SLACK_CHANNEL` to point at your slack channel.

