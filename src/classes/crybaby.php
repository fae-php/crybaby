<?php

namespace FAE\crybaby;

use FAE\logging\logger;

class crybaby
{

  public static function slack(string $message)
  {
    if (!empty($_ENV['CRYBABY_SLACK_CHANNEL'])) {
      $data_string = json_encode(['text' => $message]);

      $ch = curl_init($_ENV['CRYBABY_SLACK_CHANNEL']);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($data_string)
        )
      );

      $result = curl_exec($ch);
      curl_close($ch);
    }
  }


  /**
   * Default exception handler
   */
  public static function exceptionHandler($exception)
  {
    global $config;

    http_response_code(500);

    print_r($exception->getMessage());

    $message = $exception->getMessage() . " [" . $exception->getFile() . ":" . $exception->getLine() . "]";

    logger::log()->critical($message, $exception->getTrace());

    self::slack("```\n{$message}\n```\n on " . gethostname() . ' (' . $config->namespace  . ')');
  }

  public static function errorHandler($errno, $errstr, $errfile, $errline)
  {
    global $config;

    if (!(error_reporting() & $errno)) {
      // This error code is not included in error_reporting
      return;
    }

    $message = "PHP [{$errno}] {$errstr} in {$errfile}:{$errline}";

    switch ($errno) {
      case E_PARSE:
      case E_ERROR:
      case E_CORE_ERROR:
      case E_COMPILE_ERROR:
      case E_USER_ERROR:
        logger::log()->error($message);
        self::slack("```\n{$message}\n```\n on " . gethostname() . ' (' . $config->namespace . ')'); // Fatal errors should get slack nudge if enabled
        break;

      case E_WARNING:
      case E_CORE_WARNING:
      case E_COMPILE_WARNING:
      case E_USER_WARNING:
        logger::log()->warning($message);
        break;

      case E_STRICT:
      case E_NOTICE:
      case E_DEPRECATED:
      case E_USER_DEPRECATED:
      case E_STRICT:
      case E_USER_NOTICE:
        logger::log()->notice($message);
        break;

      default:
        logger::log()->notice("Unknown error type: {$message}");
        break;
    }

    /* Allow the standard error handlers to continue */
    return false;
  }

  /**
   * This is a hook to catch unhandled fatal errors - parsing errors, fatal errors, etc etc.
   */
  public static function unhandledErrorHandler()
  {
    register_shutdown_function(function () {

      global $config;

      $error = error_get_last();
      if (isset($error["type"]) && $error["type"] == E_ERROR) {


        try {
          ob_clean(); // Clear out any existing output buffer.
        } catch (\ErrorException $e) {
        }

        http_response_code(500);

        if (!empty($_SERVER['SERVER_NAME'])) {
          $server_name = $_SERVER['SERVER_NAME'];
        } else {
          $server_name = '';
        }
        if (!empty($_SERVER['REQUEST_URI'])) {
          $request_uri = $_SERVER['REQUEST_URI'];
        } else {
          $request_uri = '';
        }

        $error = "Fatal Error: {$error['file']}:{$error['line']} - \"{$error['message']}\", on page {$server_name}{$request_uri}";

        logger::log()->error($error);

        // Lets slack the Krew
        try {
          self::slack("```\n{$error}\n```\n on " . gethostname() . ' (' . $config->namespace . ')');
        } catch (\Exception $ex) {
        }

        die($error);

        exit;
      }
    });
  }
}


set_exception_handler(__NAMESPACE__ . '\\crybaby::exceptionHandler');
set_error_handler(__NAMESPACE__ . '\\crybaby::errorHandler');
crybaby::unhandledErrorHandler(); // Handle unhandled fatal errors

